import React from 'react';
import { ToDoItem } from './todoitem';

export class TodoList extends React.Component {

    filterTasks() {
        let filteredTasks = [];
        if (this.props.filter === "all") {
            filteredTasks = this.props.tasks;

        } else {
            filteredTasks = this.props.tasks.filter((item) => {
                return item.status === this.props.filter;
            });
        }
        return filteredTasks;

    }

    render() {
        let filteredTasks = this.filterTasks();

        return <div className="list">
            <ul>
                {
                    filteredTasks.map(
                        (elem, index) => <ToDoItem key={index}
                            index={index}
                            status={elem.status}
                            title={elem.title}
                            changeTask={this.props.changeTask}
                            removeTask={this.props.removeTask}
                        />)
                }
            </ul>
        </div>;
    }
}

