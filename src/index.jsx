import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './main';

// var tasksList = ["Помыть посуду", "Сходить в магазин", "Приготовить ужин", "Task 4", "Task 5"];
var tasksList2 = [
  {
    title: "Помыть посуду",
    status: true
  },
  {
    title: "Сходить в магазин",
    status: false
  },
  {
    title: "Приготовить ужин",
    status: false
  },
  {
    title: "Постирать",
    status: false
  },
  {
    title: "Погладить рубашку",
    status: true
  }
];


ReactDOM.render(
  <App tasks={tasksList2} />,
  document.getElementById('root')
);