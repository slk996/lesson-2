import React from 'react';
import { App } from './index';

export class TodoInput extends React.Component {

  handleSubmit = (event) => {
    event.preventDefault(); //отменяем отправку формы
    var input = event.target.querySelector('input');
    var value = input.value;
    if (input.value.length > 0) {
      input.value = '';
      var add = this.props.addTask;
      add(value);
    }

  }


  render() {
    //const css = { color: 'blue', fontSize: '20px', width: '100%' };
    return <form onSubmit={this.handleSubmit} className="input">
      <input type="text" placeholder='What needs to be done?' />
      {/* <input type="submit" /> */}
    </form>;
  }
}