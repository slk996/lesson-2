import React from 'react';

export class Footer extends React.Component {
    constructor() {
        super();
    }

    render() {
        return <div className="filters">
            <button className="btn" onClick={this.props.filterToggle.bind(null, "all")}>ALL</button>
            <button className="btn" onClick={this.props.filterToggle.bind(null, true)}>Completed</button>
            <button className="btn" onClick={this.props.filterToggle.bind(null, false)}>Active</button>
            <button className="btnClear" onClick={this.props.clearCompleted}>clear completed</button>
        </div>
    }

}