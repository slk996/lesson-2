import React from 'react';
import { TodoInput } from './todoinput';
import { TodoList } from './todolist';
import { Footer } from './footer';


export class App extends React.Component {
    constructor(props) {
        super();
        this.state = { defaultTasks: props.tasks, filter : "all" };

    }

    addToList = (text) => {
        var wrapper = { status: false };
        wrapper.title = text;

        var tasksForUpdate = this.state.defaultTasks;
        tasksForUpdate.push(wrapper);
        this.setState({ fefaultTasks: tasksForUpdate });
    }

    delTask = (text) => {
        console.log(text);
        console.log(this.state.defaultTasks.indexOf(text));
        // let text = elem.target.parentNode.querySelector('span').innerText;
        var tasksForUpdate = this.state.defaultTasks;
        // tasksForUpdate.splice(tasksForUpdate.indexOf(text), 1);
        tasksForUpdate.forEach((elem, index)=>{
            if (elem.title === text) {
                tasksForUpdate.splice(index, 1); 
                return;   
            }
        });
        this.setState({ defaultTasks: tasksForUpdate });

    }

    changeTask = (text) => {
        var tasksForUpdate = this.state.defaultTasks;
        tasksForUpdate.forEach((elem, i) => {
            if (elem.title === text) {
                elem.status = !elem.status;
                return;
            }
        });
        this.setState({ defaultTasks: tasksForUpdate });

    }



    clearCompleted = () => {
        let tasksForUpdate = this.state.defaultTasks.filter((item) => {
            return item.status === false;
        });
        this.setState({ defaultTasks: tasksForUpdate });

    }

    filterToggle = (param) => {
        console.log(param);
        // this.state.filter = param;
        this.setState({ filter: param });
    }

    render() {
        return <div>
            <TodoInput addTask={this.addToList} />
            <TodoList tasks={this.state.defaultTasks}
                filter={this.state.filter}
                removeTask={this.delTask}
                changeTask={this.changeTask}
            />
            <Footer 
                clearCompleted={this.clearCompleted}
                filter={this.state.filter}
                filterToggle={this.filterToggle}
            />
        </div>
    }
}