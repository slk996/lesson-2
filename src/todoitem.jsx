import React from 'react';

export class ToDoItem extends React.Component {

    handleClick = () => {
        this.props.removeTask(this.props.title);
    }

    handleClickChangeTask = () =>{
        this.props.changeTask(this.props.title);
    }

    render() {
        let fooId = "foo" + this.props.index;
        return <li>
            <div className="checkTask">
                <input type="checkbox" id={fooId} checked={this.props.status}
                    onChange={this.handleClickChangeTask} />
                <label htmlFor={fooId}></label>
            </div>
            <div className="textTask">
                <span className={this.props.status && "taskTrue"}>{this.props.title}</span>
            </div>

            <button className="butDelTask" onClick={this.handleClick}>X</button>

        </li >

    }
}

