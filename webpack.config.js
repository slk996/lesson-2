const path = require('path');

module.exports = {
    entry: './src/index.jsx',
    devtool: 'source-map',
    output: {
        path: path.resolve(__dirname, 'html'),
        filename: 'bundle.js'
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 100
    },
    module: {
        rules: [{
            test: /\.js?x$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader'
            },
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    devServer: {
        contentBase: path.join(__dirname, "html"),
        compress: true,
        port: 9000,
        proxy: {
            '/api/*': {
                target: 'http://localhost:9001',
                secure: false
            }
        }
    },
}
